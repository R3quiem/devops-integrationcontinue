<?php
require_once './src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once('./src/City.php');
class CityTest extends \PHPUnit\Framework\TestCase
{
   public function testGetCityNameById()
   {
       $city = new City();
       $result = $city->getCityNameById(1);
       $expected = 'Bordeaux';
       $this->assertTrue($result == $expected);
   }
   public function testGetParisById()
   {
       $city = new City();
       $result = $city->getCityNameById(2);
       $expected = 'Paris';
       $this->assertTrue($result == $expected);
   }
}