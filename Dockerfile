FROM liliancal/ubuntu-php-apache
ADD src/formation-git-template-html /var/www/
EXPOSE 80
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]